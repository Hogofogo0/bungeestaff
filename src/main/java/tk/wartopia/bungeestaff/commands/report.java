package tk.wartopia.bungeestaff.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import tk.wartopia.bungeestaff.Bungeestaff;

import java.util.HashMap;
import java.util.UUID;

public class report extends Command {

    Bungeestaff pl;




    public report(){
        super("report", "bungee.report", "rep", "wdr");
    }


    @Override
    public void execute(CommandSender sender, String[] args) {

        HashMap<String, Long> cooldowns = new HashMap<String, Long>();


        int cooldown = 60;
        if (cooldowns.containsKey(sender.getName())) {
            long secondsLeft = ((cooldowns.get(sender.getName()) / 1000) + cooldown) - (System.currentTimeMillis() / 1000);
            if (secondsLeft > 0) {
                // Still cooling down
                sender.sendMessage(new TextComponent(ChatColor.RED + "You cant use that commands for another " + ChatColor.RED + "" + ChatColor.BOLD + secondsLeft + ChatColor.RED + " seconds!"));
                return;
            }
                cooldowns.put(sender.getName(), System.currentTimeMillis());





        }else if (args.length > 2) {

        ProxiedPlayer t = Bungeestaff.instance.getProxy().getPlayer(args[0]);
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < args.length; i++) {

            sb.append(args[i]);
            sb.append(" ");

        }
        String reason = sb.toString();

        for (int i = 0; i < pl.staffenabledreport.size(); i++) {

            ProxiedPlayer staffmember = pl.staffenabledreport.get(i);

            String repplayerformat = pl.database.getConfig().getString("repplayerformat").replaceAll("%reported%", t.getName());
            String reasonformat = pl.database.getConfig().getString("reasonformat").replaceAll("%reason%", reason);
            String reportedbyformat = pl.database.getConfig().getString("reportedbyformat").replaceAll("%author%", sender.getName());


            staffmember.sendMessage(new TextComponent(ChatColor.RED + "" + ChatColor.BOLD + "-------------------------------------------------------------"));
            staffmember.sendMessage(new TextComponent(""));
            staffmember.sendMessage(new TextComponent(repplayerformat));
            staffmember.sendMessage(new TextComponent(reasonformat));
            staffmember.sendMessage(new TextComponent(reportedbyformat));
            staffmember.sendMessage(new TextComponent(""));
            staffmember.sendMessage(new TextComponent(ChatColor.RED + "" + ChatColor.BOLD + "-------------------------------------------------------------"));
            sender.sendMessage(new TextComponent(ChatColor.GREEN + "Player has been reported! Thank you!"));

        }



        }
    }
}
