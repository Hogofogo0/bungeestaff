package tk.wartopia.bungeestaff;


import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class database {

    private String configname = "database.yml";

    private Bungeestaff plugin;
    private Configuration dataConfig = null;
    private File configFile = null;

    public database(Bungeestaff plugin) {
        this.plugin = plugin;
        saveDefaultConfig();
    }

    public void reloadConfig() {
        if(this.configFile == null)
            this.configFile= new File(this.plugin.getDataFolder(), configname);

        try {
            if (!configFile.exists()) {
                configFile.createNewFile();
            }
            dataConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Configuration getConfig() {
        if(this.dataConfig == null)
            reloadConfig();
        return this.dataConfig;
    }

    public void saveConfig() {
        if(this.dataConfig == null || this.configFile == null)
            return;

        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(dataConfig, this.configFile);
        } catch (IOException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + this.configFile, e);
        }

    }

    public void saveDefaultConfig() {
        if(this.configFile == null)
            this.configFile= new File(this.plugin.getDataFolder(), configname);

        try {
            if (!configFile.exists()) {
                configFile.createNewFile();
            }
            dataConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
