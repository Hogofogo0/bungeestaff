package tk.wartopia.bungeestaff.staffchat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import tk.wartopia.bungeestaff.Bungeestaff;

public class staffchat implements Listener {

    Bungeestaff pl;

    @EventHandler
    public void staffchatevent(ChatEvent e){

        ProxiedPlayer p = (ProxiedPlayer) e.getSender();

        if(e.getMessage().startsWith(pl.database.getConfig().getString("staffchattrigger"))){


            if(p.hasPermission("staffchat.use")){

                for(int i = 0; i < pl.staffenabledchat.size(); i++){

                    String new_msg = ChatColor.translateAlternateColorCodes('&', pl.database.getConfig().getString("staffchatformat").replaceAll("%player%", p.getName())
                                            .replaceAll("%message%", e.getMessage()));

                    ProxiedPlayer staffmember = pl.staffenabledchat.get(i);
                    staffmember.sendMessage(new TextComponent(new_msg));


                }

                e.setCancelled(true);

            }



        }


    }


}
