package tk.wartopia.bungeestaff.staffchat;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import tk.wartopia.bungeestaff.Bungeestaff;

public class togglestaffchat extends Command {

    Bungeestaff pl;

    public togglestaffchat(){

        super("staffcchattoggle", "staffchat.toggle", "st", "stafftoggle", "staffct");

    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        ProxiedPlayer p = (ProxiedPlayer) sender;
        if(pl.staffenabledchat.contains(p)){

            p.sendMessage(new TextComponent("Staffchat disabled!"));
            pl.staffenabledchat.remove(p);

        }else{

            p.sendMessage(new TextComponent("Staffchat enabled!"));
            pl.staffenabledchat.add(p);

        }

    }
}
