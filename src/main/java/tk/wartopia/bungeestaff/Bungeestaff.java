package tk.wartopia.bungeestaff;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import tk.wartopia.bungeestaff.commands.report;
import tk.wartopia.bungeestaff.commands.togglereport;
import tk.wartopia.bungeestaff.events.ReportJoinEvent;
import tk.wartopia.bungeestaff.staffchat.enablejoin;
import tk.wartopia.bungeestaff.staffchat.staffchat;
import tk.wartopia.bungeestaff.staffchat.togglestaffchat;

import java.util.ArrayList;


public final class Bungeestaff extends Plugin {


    public ArrayList<ProxiedPlayer> staffenabledreport = new ArrayList<>();
    public ArrayList<ProxiedPlayer> staffenabledchat = new ArrayList<>();
    public static Bungeestaff instance;
    public database database;
    public static Bungeestaff plugin;





    @Override
    public void onEnable() {



        database.getConfig().set("repplayerformat", ChatColor.BLUE + "" + ChatColor.BOLD + "Reported player: " + ChatColor.RED + "%reported%");
        database.getConfig().set("reasonformat", ChatColor.BLUE + "" + ChatColor.BOLD + "For: " + ChatColor.GREEN + "%reason%");
        database.getConfig().set("reportedbyformat", ChatColor.BLUE + "" + ChatColor.BOLD + "By: " + ChatColor.GOLD + "%author%");
        database.getConfig().set("staffchattrigger", "#");
        database.getConfig().set("staffchatformat", "&d&lSTAFFCHAT | &r %player%: %message%");
        database.saveConfig();
        getProxy().getPluginManager().registerCommand(this, new report());
        getProxy().getPluginManager().registerListener(this, new ReportJoinEvent());
        getProxy().getPluginManager().registerCommand(this, new togglereport());
        getProxy().getPluginManager().registerCommand(this, new togglestaffchat());
        getProxy().getPluginManager().registerListener(this, new staffchat());
        getProxy().getPluginManager().registerListener(this, new enablejoin());









    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
